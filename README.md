There are few coding challenges. Once you are ready to show your solutions, please send me the invitation or link to your GitHub/Bitbucket repository. 


Notice: Challenges from 1 to 4 should be covered by unit tests.

Good luck!


**Challenge #1**

Your task is to take three letters from given string(without any restrictions to chars) while prioritizing all big ones.
When there is not enough of big letters or they are missing, you should take the small ones instead. 
The returned string should contain only three big letters.

Examples:
```
"Test Me Please" => "TMP"
"TEst me please" => "TES"
"Tst Me please" => "TMS"
"tEst Me please" => "EMT"
"test Me please" => "MTE"
"test me please" => "TES"
"test me pleasE" => "ETE"
```

**Challenge #2**

Your task is to sort a given string. Each word in the string will contain a single number. This number is the position the word should have in the result.

Notes: 
1. Numbers can be from 1 to 9. So 1 will be the first word (not 0).
2. There won't be any numbers duplications like `is2 4a is2`.

If the input string is empty, return an empty string. The words in the input string will only contain valid consecutive numbers.

For an input: "is2 Thi1s T7est 4a" the function should return "Thi1s is2 4a T7est"

**Challenge #3**

There is an array of strings. All strings contains similar letters except one. Try to find it!

```
[ 'Aa', 'aaa', 'aaaaa', 'BbBb', 'Aaaa', 'AaAaAa', 'a' ] => 'BbBb'
[ 'abc', 'acb', 'bac', 'foo', 'bca', 'cab', 'cba' ] => 'foo'
[ 'silvia', 'vasili', 'victor' ]; => victor
[ 'Tom Marvolo Riddle', 'I am Lord Voldemort', 'Harry Potter' ] => Harry Potter
[ '     ', 'a', ' ' ] => a
```

Strings may contain spaces. Spaces is not significant, only non-spaces symbols matters. E.g. string that contains only spaces is like empty string.

It's guaranteed that array contains more than 3 strings.


**Challenge #4**

Management wants to have some query searcher in their site. When user will pass this kind of string:

```
term:value term2:"value2" term3:"value value" term4:"value : value : value"
```

he should get:
```
[
	"term" => "value",
	"term2" => "value2",
	"term3" => "value value",
	"term4" => "value : value : value"
]
```

You should accept two parameters:
- array of available terms(like 'term', 'term2')
- string to parse

Error handling:
1. When term from given string is not available - throw an exception
2. When given string is invalid(like e.g.: `value value` - without giving term` - throw an exception

Some examples:

```
parseSearchQuery(['term', 'term1'], 'term:value term2:"value2"'); => We're getting exception as used 'term2' is not available
parseSearchQuery(['term', 'term2'], 'term:value term2:"value2:value test"') => ["term" => "value", "term2" => "value2:value test"]
```



**Challenge #5**

Your task is to write unit tests for every class excluding `Config.php`. You are allowed to change code source only in  `SearchTermsCreator` class.


**Challenge #6**

Your task is to perform code review. Try to find as much of bad points as possible and propose how it should be resolved instead.
