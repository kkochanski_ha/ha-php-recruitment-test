<?php


class TagTermFilter
{
    /**
     * @param array $tags
     *
     * @return array
     */
    public function filter(array $tags)
    {
        return array_filter($tags, function($value) {
            return !in_array($value, [null, '', 'dummy', 'qwerty']);
        });
    }
}
