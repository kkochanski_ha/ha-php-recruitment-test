<?php


class SearchTermsCreator
{
    private $data;
    private $config;

    /**
     * SearchTermsCreator constructor.
     *
     * @param array $data
     */
    public function __construct(array $data)
    {
        $this->data = $data;
        $this->config = Config::get();
    }

    /**
     * @return array|null
     */
    public function prepareSearchData()
    {
        if(empty($this->config['allow_preparing_search_data'])) {
            return null;
        }

        $result = [];

        if(isset($this->data['tags'])) {
            $tagTermFilter = new TagTermFilter();
            $result['tags'] = $tagTermFilter->filter($this->data['tags']);
            unset($this->data['tags']);
        }


        $termFilter = new TermFilter();
        foreach($this->data as $key => $value) {
            $result[$key] = $termFilter->filter($value);
        }

        return $result;
    }
}
