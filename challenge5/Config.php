<?php


class Config
{
    protected static $config;

    /**
     * @return array
     */
    public static function get()
    {
        if (!isset(static::$config)) {
            static::$config = ['allow_preparing_search_data' => true];
        }

        return static::$config;
    }
}
