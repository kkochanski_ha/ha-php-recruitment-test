<?php


class TermFilter
{
    /**
     * @param array $termData
     *
     * @return array
     */
    public function filter(array $termData)
    {
        return array_filter(array_map('intval', $termData), function($value) {
            return !in_array($value, array_merge(range(0, 10), range(50, 100)));
        });
    }
}
